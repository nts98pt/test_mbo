<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalAttitude extends Model
{
    protected $guarded = ['id'];
    protected $table = 'appraisal_jobresults';

    protected $fillable = [
        'task_type',
        'dificult',
        'weight',
        'appraisee_answer',
        'appraiser_answer',
        'reviewer_answer',
        'appraisee_comment',
        'appraiser_comment',
        'reviewer_comment',
        'appraisee_score',
        'appraiser_score',
        'reviewer_score',
        'appraisal_id',

    ];
    public function Appraisal(){
        return $this->belongsTo(Appraisal::class,'appraisal_id');
    }
}

