<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalJobresults extends Model
{
    protected $guarded = ['id'];
    protected $table = 'appraisal_attitudes';

    protected $fillable = [
        'appraisee_answer',
        'appraiser_answer',
        'reviewer_answer',
        'appraisee_comment',
        'appraiser_comment',
        'reviewer_comment',
        'appraisal_id',

    ];
    public function appraisal(){
        return $this->hasMany(Appraisal::class,'appraisal_id');
    }
    public function project(){
        return $this->hasOne(Project::class,'appraisal_id');
    }
}
