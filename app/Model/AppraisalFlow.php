<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppraisalFlow extends Model
{
    protected $guarded = ['id'];
    protected $table = 'appraisal_flows';

    protected $fillable = [
        'appraiser_id',
        'reviewer_id',
    ];
}
