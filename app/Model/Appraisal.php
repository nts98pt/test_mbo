<?php

namespace App;

use App\Employee;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\User;


class Appraisal extends Model
{
    use HasFactory;

    protected $guarded = ['id'];
    protected $table = 'appraisals';

    protected $fillable = [
        'status',
        'create_at',
        'approved_date',
        'deleted_at',
        'updated_at',
        'appraisee_comment',
        'appraiser_comment',
        'reviewer_comment',
        'appraisee_score',
        'appraiser_score',
        'reviewer_score',
        'employee_id',
    ];
    public function employee()
    {
        return $this->hasMany(Employee::class, 'employee_id');
    }
}
