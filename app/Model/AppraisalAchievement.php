<?php

namespace App;

use App\Models\Appraisal;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class AppraisalAchievement extends Model
{
    protected $guarded = ['id'];
    protected $table = 'appraisal_achievements';

    protected $fillable = [
        'appraisee_answer',
        'appraiser_answer',
        'reviewer_answer',
        'appraisee_comment',
        'appraiser_comment',
        'reviewer_comment',
        'appraisal_id',

    ];
    public function Appraisal(){
        return $this->belongsTo(\App\Appraisal::class,'appraisal_id');
    }
}
