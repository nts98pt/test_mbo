<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $guarded = ['id'];
    protected $table = 'employees';

    protected $fillable = [
        'appraisal_flow_id',

    ];
    public function appraisalFlow(){
        return $this->hasOne(AppraisalFlow::class,'appraisal_id');
    }
}
