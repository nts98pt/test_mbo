<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalJobresultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_jobresults', function (Blueprint $table) {
            $table->id();
            $table->integer('task_type');
            $table->string('dificult', 200);
            $table->string('weight',200);
            $table->json('appraisee_answer');
            $table->json('appraiser_answer');
            $table->json('reviewer_answer');
            $table->string('appraisee_comment',600);
            $table->string('appraiser_comment',600);
            $table->string('reviewer_comment',600);
            $table->float('appraisee_score');
            $table->float('appraiser_score');
            $table->float('reviewer_score');
            $table->unsignedBigInteger('appraisal_id');
            $table->unsignedBigInteger('project_id');
            $table->timestamps();

            $table->foreign('appraisal_id')->references('id')->on('appraisals')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_jobresults');
    }
}
