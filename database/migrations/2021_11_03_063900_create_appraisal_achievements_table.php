<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_achievements', function (Blueprint $table) {
            $table->id();
            $table->json('appraisee_answer');
            $table->json('appraiser_answer');
            $table->json('reviewer_answer');
            $table->json('appraisee_comment');
            $table->json('appraiser_comment');
            $table->json('reviewer_comment');
            $table->unsignedBigInteger('appraisal_id');
            $table->timestamps();

            $table->foreign('appraisal_id')->references('id')->on('appraisals')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_achievements');
    }
}
