<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisals', function (Blueprint $table) {
            $table->id();
            $table->integer('status');
            $table->dateTime('create_at');
            $table->dateTime('approved_date');
            $table->dateTime('deleted_at');
            $table->dateTime('updated_at');
            $table->string('appraisee_comment',600);
            $table->string('appraiser_comment',600);
            $table->string('reviewer_comment',600);
            $table->string('appraisee_score');
            $table->string('appraiser_score');
            $table->string('reviewer_score');
            $table->unsignedBigInteger('employee_id');
            $table->timestamps();

            $table->foreign('employee_id')->references('id')->on('employees')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisals');
    }
}
